#
# Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#  blob(s) necessary for AM65x hardware

# GPU kernel module
BOARD_VENDOR_KERNEL_MODULES += \
	vendor/ti/am65x/sgx_km/lib/modules/pvrsrvkm.ko \

# GPU userspace libraries
PRODUCT_COPY_FILES += \
	vendor/ti/am65x/sgx_um/bin/pvrsrvctl:vendor/bin/pvrsrvctl:ti \
	vendor/ti/am65x/sgx_um/lib64/egl/libEGL_POWERVR_SGX544_117.so:vendor/lib64/egl/libEGL_POWERVR_SGX544_117.so:ti \
	vendor/ti/am65x/sgx_um/lib64/egl/libGLESv1_CM_POWERVR_SGX544_117.so:vendor/lib64/egl/libGLESv1_CM_POWERVR_SGX544_117.so:ti \
	vendor/ti/am65x/sgx_um/lib64/egl/libGLESv2_POWERVR_SGX544_117.so:vendor/lib64/egl/libGLESv2_POWERVR_SGX544_117.so:ti \
	vendor/ti/am65x/sgx_um/lib64/gbm_pvr.so:vendor/lib64/gbm_pvr.so:ti \
	vendor/ti/am65x/sgx_um/lib64/hw/gralloc.am65x.so:vendor/lib64/hw/gralloc.am65x.so:ti \
	vendor/ti/am65x/sgx_um/lib64/hw/memtrack.am65x.so:vendor/lib64/hw/memtrack.am65x.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libdbm.so:vendor/lib64/libdbm.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libglslcompiler.so:vendor/lib64/libglslcompiler.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libIMGegl.so:vendor/lib64/libIMGegl.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libpvr2d.so:vendor/lib64/libpvr2d.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libpvrANDROID_WSEGL.so:vendor/lib64/libpvrANDROID_WSEGL.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libPVRScopeServices.so:vendor/lib64/libPVRScopeServices.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libsrv_init.so:vendor/lib64/libsrv_init.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libsrv_um.so:vendor/lib64/libsrv_um.so:ti \
	vendor/ti/am65x/sgx_um/lib64/libusc.so:vendor/lib64/libusc.so:ti \
	vendor/ti/am65x/sgx_um/lib/egl/libEGL_POWERVR_SGX544_117.so:vendor/lib/egl/libEGL_POWERVR_SGX544_117.so:ti \
	vendor/ti/am65x/sgx_um/lib/egl/libGLESv1_CM_POWERVR_SGX544_117.so:vendor/lib/egl/libGLESv1_CM_POWERVR_SGX544_117.so:ti \
	vendor/ti/am65x/sgx_um/lib/egl/libGLESv2_POWERVR_SGX544_117.so:vendor/lib/egl/libGLESv2_POWERVR_SGX544_117.so:ti \
	vendor/ti/am65x/sgx_um/lib/gbm_pvr.so:vendor/lib/gbm_pvr.so:ti \
	vendor/ti/am65x/sgx_um/lib/hw/gralloc.am65x.so:vendor/lib/hw/gralloc.am65x.so:ti \
	vendor/ti/am65x/sgx_um/lib/hw/memtrack.am65x.so:vendor/lib/hw/memtrack.am65x.so:ti \
	vendor/ti/am65x/sgx_um/lib/libdbm.so:vendor/lib/libdbm.so:ti \
	vendor/ti/am65x/sgx_um/lib/libglslcompiler.so:vendor/lib/libglslcompiler.so:ti \
	vendor/ti/am65x/sgx_um/lib/libIMGegl.so:vendor/lib/libIMGegl.so:ti \
	vendor/ti/am65x/sgx_um/lib/libpvr2d.so:vendor/lib/libpvr2d.so:ti \
	vendor/ti/am65x/sgx_um/lib/libpvrANDROID_WSEGL.so:vendor/lib/libpvrANDROID_WSEGL.so:ti \
	vendor/ti/am65x/sgx_um/lib/libPVRScopeServices.so:vendor/lib/libPVRScopeServices.so:ti \
	vendor/ti/am65x/sgx_um/lib/libsrv_init.so:vendor/lib/libsrv_init.so:ti \
	vendor/ti/am65x/sgx_um/lib/libsrv_um.so:vendor/lib/libsrv_um.so:ti \
	vendor/ti/am65x/sgx_um/lib/libusc.so:vendor/lib/libusc.so:ti \
